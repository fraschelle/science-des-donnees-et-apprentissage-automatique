#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Linear regression.
"""
from typing import Sequence
import numpy as np
from scipy.optimize import root

def least_square(x: Sequence[float], y: Sequence[float]):
    """
    Take two series of floats (ideally, numpy array compatible objects) and returns
    the coefficients alpha and beta (two floats) corresponding to the linear
    regression
        y = alpha + beta * x
    
    """
    if len(x) != len(y):
        raise ValueError("x and y must have same size")
    x, y = np.array(x), np.array(y)
    xm, ym = np.mean(x), np.mean(y)
    beta = np.sum((x-xm)*(y-ym))/np.sum((x-xm)*(x-xm))
    alpha = ym - beta*xm
    return alpha, beta

def add_x0(x: np.array):
    """
    Add a first column of 1 on the data, in order to fit the regression coefficients.
    
    Parameters:
        x: a 2D numpy array, with data in columns (first argument of x.shape is the number of exmaples, second argument of x.shape is the number of features)
    
    Output:
        a numpy array, of shape (x.shape[0], x.shape[1] +1)
    """
    assert x.ndim == 2, "Input array must be of dimension 2"
    z = np.full(x.shape[0],1).reshape(x.shape[0],1)
    z = np.concatenate([z,x], axis=1)
    return z

def linear_regression(x: np.array, y: np.array):
    """Calculate the coefficients of the linear regression with least square method. The dimension of the problem is """
    assert x.ndim == 2, "Input features array must be of dimension 2"
    assert y.ndim == 1, "Input target array must be of dimension 1"
    assert x.shape[0] == len(y), "Number of examples and targets must be the same"
    x = add_x0(x)
    return np.linalg.inv(x.T @ x) @ x.T @ y

def p(beta, x):
    """
    Calculate the density probability of a logistic regression, of coefficients beta and data x
    """
    assert beta.shape[0] == x.shape[1], "Wrong match of the array, len of beta must be the same of the number of columns in x. Try transposing x for testing."
    z = np.sum(beta * x, axis=1)
    z = 1 / (1 + np.exp(-z))
    return z

def eqs(beta, x, y):
    """
    Equation to be resolved for the logistic regression

    Parameters
    ----------
    beta : a 1D numpy array, or a list
        the parameter of the logistic regression, which have to be found by optimization protocol.
    x : a 2D numpy array
        the data.
    y : a 1D numpy array
        the target of the regression.

    Returns
    -------
    z : a 1D numpy array
        the numerical equations corresponding to the maximization of the likelihood.
        one value per entry in the beta array

    """
    z = p(beta, x)
    z = y.flatten() - z
    z = np.tile(z, (2,1)).T
    z = np.sum(x*z, axis=0)
    return z

def jac(beta, x, y):
    """
    The Jacobian of the likelihood, for use in the root finding algorithm
    """
    jacobian = np.zeros((x.shape[1], x.shape[1]))
    dp = p(beta, x) * (1 - p(beta, x))
    for i in range(x.shape[1]):
        for j in range(x.shape[1]):
            jacobian[i,j] = np.sum(x[:,i]*x[:,j]*dp)
    return -jacobian

def Phi(x):
    """
    Calculate the density probability of a logistic regression, of coefficients beta and data x
    """
    z = 1 / (1 + np.exp(-x))
    return z

def logistic_regression(x, y,
                        start=None,
                        precision=float('1e-4'),
                        gamma=float('1e-6')):
    """
    Calculate the coefficients of the logistic regression, with data x and target y.

    Parameters
    ----------
    x : a numpy array
        the data, in columns.
    y : a numpy array
        the target, incolumns.
    start : a list or a numpy array, optional
        Starting guess of the parameters, in order to initialize the root finding algorithm.
        Must be of the length of x.shape[1]. 
        The default is None, in which case the initial guess are random.

    Returns
    -------
    coeffs : dictionnary of arrays
        coeffs['x'] corresponds to the coefficients of the regression, in the form
            [intercept, beta1, beta2, ...]
        coeffs['fjac'] corresponds to the jacobian at the convergence point
    
    Returns an error in case of non convergence of the root finding algorithm.

    """
    assert y.shape[0] == x.shape[0], "y and x nb of rows must match"
    if start is None:
        alpha = np.random.random(size=x.shape[1]+1).reshape(x.shape[1]+1, 1)
    else:
        alpha = start
    # add a col of 1 before the datas
    z = np.full(x.shape[0], 1).reshape(x.shape[0], 1)
    x = np.concatenate([z,x], axis=1)
    pres = 1
    while pres > precision:
        pres = gamma * x.T @ (1 / (1 + np.exp(-x@alpha)) - y)
        alpha -= pres
        pres = np.sum(np.sqrt(pres**2))
    return alpha
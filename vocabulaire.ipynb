{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "990edcc9",
   "metadata": {},
   "source": [
    "# Méthodologie et vocabulaire associé\n",
    "\n",
    "On survole les mots clés importants de l'apprentissage automatique. L'intérêt est de faire de ce vocabulaire une base d'exploratin des différents concepts de ce domaine foisonnant et riche."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa29955c",
   "metadata": {},
   "source": [
    "Dans l'exemple de la classification (à 2D et deux classes), on a un ensemble de points $\\left\\{x_{i}^{1}, x_{i}^{2}\\right\\}_{i=1\\cdots N}$ et un ensemble de labels ne pouvant prendre que deux valeurs discrètes $0$ ou $1$, $\\left\\{y_{i}\\right\\}_{i=1\\cdots N}\\in\\left\\{0,1\\right\\}^{N}$\n",
    "\n",
    "![classification](fig/classification-algorithm-in-machine-learning.png)\n",
    "\n",
    "On souhaite que l'algorithme attribue tout seul une de ces deux catégories $Y=0$ ou $Y=1$ à n'importe quel point $X=\\left(x^1, x^2\\right)$ donné. Cela revient à essayer de trouver la fonction $\\Phi$ telle que $\\Phi\\left(X\\right) = Y$. \n",
    "\n",
    "Dans la pratique, la fonction $\\Phi$ est connue partiellement ; au moins sa forme. Par exemple si les données ressemblent à une droite, on va naturellement étudier le modèle $\\Phi\\left(x\\right) = \\alpha_0 + \\alpha_1 x$. L'enjeux est alors de découvrir les paramètres $\\alpha_0$ et $\\alpha_1$. Dans le cas général, l'ensemble des paramètres est représenté par le vecteur (ou la matrice) $\\boldsymbol{\\alpha}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89384e89",
   "metadata": {},
   "source": [
    "## Exemples de choix, _Gold Standard_\n",
    "\n",
    "Pour obtenir les paramètres qui s'adaptent au mieux au problème posé, on dit qu'on **entraîne** (_train_) l'algorithme de découverte de $\\Phi$, ou que l'on **ajuste** (_fit_) le modèle $\\Phi$. \n",
    "\n",
    "Pour entraîner l'algorithme, ou ajuster le modèle, on a besoin d'exemples. Trouver de bons exemples est un enjeux majeur de la plupart des problèmes de data science. Un bon jeu d'exemples s'appelle un **Gold Standard**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4df4ae3",
   "metadata": {},
   "source": [
    "## Choix des variables, _features_ et _target_ (_label_ ou _category_) et _preprocessing_\n",
    "\n",
    "L'essentiel du travail préparatoire du data scientist consiste à trouver les variables pertinentes permettant à un algorithme de résoudre le problème. Cela s'appelle sélectionner les bonnes **features**, notées $X$ (c'est un vecteur de grande dimension en général, ou une matrice si on regarde l'ensemble des exemples). La variable à prédire s'appelle **target**, notée $Y$. \n",
    "\n",
    "Dans l'écrasante majorité des cas, un long travail de mise en forme des données est nécessaire avant de pouvoir faire le moindre entraînement :  c'est le travail de **preprocessing**. \n",
    "\n",
    "```{admonition} Cas spécifique de l'entraînement non supervisé\n",
    ":class: caution\n",
    "\n",
    "Dans le cadre de l'entraînement non-supervisé (_unsupervised learning_, _clustering_ ou _dimensionnality reduction_), il n'y a pas de target. \n",
    "```\n",
    "\n",
    "```{admonition} Sélection des features et réductions dimensionnelles\n",
    ":class: tip\n",
    "\n",
    "Il existe des outils de sélection des features, ou de mise en pertinence des features par réduction dimensionnelle. Certains algorithmes (comme les Random Forest par exemple) sont moins sensibles aux mauvaises features, ou sélectionnent eux-mêmes les features pertinentes pour l'apprentissage.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65a42179",
   "metadata": {},
   "source": [
    "## Prédictions\n",
    "\n",
    "Une fois que l'on a entraîné un modèle, il devient déterministe. Par exemple, dans le cas de la régression linéaire, ne fois que l'on a obtenu les paramètres $\\alpha_0$ et $\\alpha_1$ qui représentent au mieux les données sur une droite $y = \\alpha_0 + \\alpha_1 x$, on peut connaître un $y$ pour n'importe quel $x$ réel. On peut donc **prédire** le résultat d'une expérience, tel que le modèle le perçoit. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "631a0853",
   "metadata": {},
   "source": [
    "## Choix du modèle d'optimisation, _loss function_ et _hyperparamètres_\n",
    "\n",
    "C'est le coeur du problème : choisir le bon algorithme à même de s'entraîner correctement, et de restituer des résultats convenables. La liste de tous les modèles est généralement très détaillée dans les différentes librairies disponibles, ce qui permet de stimuler l'intuition du data scientist concernant le choix de tel ou tel modèle d'apprentissage. \n",
    "\n",
    "Au coeur de tous les modèles de machine learning se cache une fonction de coût appelée **loss function**. Cette fonction est primordiale, puisque les algorithmes d'apprentissage ne sont que des méthodes mathématiques visant à minimiser la fonction de coût. Dit autrement, le modèle se déclare \"entraîné\" lorsqu'il n'arrive plus à baisser la valeur de la fonction de coût par rapport au nombre d'exemple qu'il ingurgite. De plus, chaque algorithme a été étudié mathématiquement, et il est prouvé qu'il converge. C'est-à-dire que chaque algorithme va forcément obtenir une solution quelque soit les données d'entraînement fournies. Reste à savoir si les résultats sont bons ! \n",
    "\n",
    "```{admonition} Convergence et réseaux de neurones artificiels\n",
    ":class: caution\n",
    "\n",
    "La convergence n'est établie que pour les algorithmes d'optimisation déterministe, comme c'est le cas pour l'ensemble des modèles de [`scikit-learn`](https://scikit-learn.org/). Cela n'est _pas_ le cas pour les modèles basés sur une descente de gradient stochastique et une rétro-propagation de gradient, comme pour les réseaux de neurones artificiels.\n",
    "```\n",
    "\n",
    "```{admonition} Apprentissage par renforcement\n",
    ":class: note\n",
    "\n",
    "Dans le cadre de l'apprentissage par renforcement, il n'y a pas de fonction de coût, mais des critères de mise à jour des coefficients des choix possibles que l'agent peut réaliser dans son environement. Dans la pratique la différence n'est pas si sensible entre ces deux notions. \n",
    "```\n",
    "\n",
    "Minimiser la fonction de coût permet de découvrir les paramètres du modèle (pensons à la régression linéaire qui contient l'ordonnée à l'origine et le coefficient directeur de la droite de régression comme paramètres) qui s'adapent au mieux aux exemples utilisés pour l'entraînement. Mais la fonction de coût peut avoir elle même des paramètres, qui vont changer les paramètres du modèle entraîné. Ces paramètres extérieurs s'appelent des **hyperparamètres** (_hyperparameters_). Correctement entraîner \n",
    "\n",
    "```{admonition} Exemple des _k-means_\n",
    ":class: note\n",
    "Dans l'algorithme (supervisés) des _k-means_, on tente de découper l'ensemble des données disponibles en $k$ sous-blocs. $k$ est alors un hyperparamètre du modèle. \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "334c0113",
   "metadata": {},
   "source": [
    "## Séparation des données, _train set_ et _test set_, _cross validation_\n",
    "\n",
    "Si les algorithmes d'apprentissage convergent dans tous les cas, on eput se questionner sur la valuer de l'apprentissage. Notamment, on peut se demander (par exemple) si un modèle ayant appris à distinguer des chats et des chiens pourra également distinguer des cochons d'inde et des perroquets ...  Pour pouvoir dire que l'on a correctement entraîné un modèle, on réalise une **cross validation**, ou **validation croisée** : on sépare le jeu complet de données initiales en deux sous-jeux : \n",
    " - le premier (en général le plus volumineux) va servir à entraîner le modèle, on l'appelle le **train set**, ou **jeu d'entraînement**\n",
    " - le second jeu va servir à tester le modèle précédemment entraîné. Ce second jeu s'appelle le **jeu de test**, ou **test set**.\n",
    "\n",
    "Ce découpage est primordial, de même que les données du jeu de test ne doivent pas avoir été utilisées pour l'entraînement, sinon la machine aurait \"triché\" en quelque sorte.\n",
    "\n",
    "Obtenir les meilleurs hyperparamètres pour un ensemble d'exemple donnés se fait à l'aide la cross-validation.\n",
    "\n",
    "```{admonition} Jeu de validation et hyperparamètres\n",
    ":class: note\n",
    "\n",
    "Dans certains cas, on rajoute un troisième jeu de données, qui ne sert qu'à la toute fin de l'analyse. Ce troisième ensemble, appelé **validation set**, permet de vérifier que les hyperparamètres sont bien ajustés.\n",
    "```\n",
    "\n",
    "Il existe plusieurs variantes de [validation croisée](https://fr.wikipedia.org/wiki/Validation_crois%C3%A9e) adaptées à l'apprentissage automatique. La validation croisée n'est d'ailleurs qu'une des nombreuses techniques d'[échantillonage statistiques](https://fr.wikipedia.org/wiki/%C3%89chantillonnage_(statistiques)). La plus simple consiste à faire tourner les jeux d'entraînement et de test. Cela permet d'entraîner plusieurs fois le modèle sur des exemples différents, et de faire des statistiques des résultats de test.\n",
    "\n",
    "```{admonition} Cas des données temporelles\n",
    ":class: warning\n",
    "\n",
    "Dans le cas des données temporelles, l'ordre (chronologique) des données est important. Dans ce cas, l'échantillonage doit se faire en préservant l'ordre chronologique autant que faire se peut.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e44f345b",
   "metadata": {},
   "source": [
    "## Validation du modèle, _score function_\n",
    "\n",
    "Les résultats des tests sont calculés sur la base d'une fonction de **score**. En général, cette fonction de score s'impose naturellement : une fonction de score correspond particulièrement bien à un modèle donné.\n",
    "\n",
    "```{admonition} Exemple de la regression linéaire\n",
    ":class: note\n",
    "Dans le cadre de la régression linéaire, la fonction de coût est la moyenne des écarts quadratiques entre les données et la droite de régression. La fonction de score naturelle associée est donc la somme des écarts quadratiques elle même. \n",
    "\n",
    "Dans ce cas, fonction de score et fonction de coût sont identiques, mais on peut tout à fait choisir une autre fonction de score évidemment. On peut aussi utiliser plusieurs fonctions de score pour avoir une idée plus claire de la situation. En revanche, la fonction de coût est forcément unique ; changer de fonction de coût revient à changer de modèle.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b17a2f1",
   "metadata": {},
   "source": [
    "## _Overfit_ et dilemne biais-variance\n",
    "\n",
    "Trouver des hyperparamètres qui marchent particulièrement bien pour des jeux d'entraînement et de test donnés peut générer un modèle qui ne s'adaptera pas aux situations ultérieures réelles. On parle alors d'**overfit** pour signifier que le modèle a parfaitement appris les exemples proposés, mais ne sais pas s'adapter à des situations plus larges. \n",
    "\n",
    "L'alternative consiste alors à relaxer un peu les hyperparamètres. Dans ce cas, on acceptera un score un peu plus mauvais, mais une meillere généralisation aux cas réels. C'est l'enjeu du [dilemne biais-variance](https://fr.wikipedia.org/wiki/Dilemme_biais-variance) (_bias-variance trade-off_). \n",
    "\n",
    "```{admonition} Biais et moyenne\n",
    ":class: note\n",
    "\n",
    "Biais et moyenne sont des quasi-synonymes en statistiques.\n",
    "```\n",
    "\n",
    "\n",
    "```{image} fig/bias_variance_trade_off_underfit.png \n",
    ":alt: exemple de large biais\n",
    ":align: center\n",
    "```\n",
    "\n",
    "Les hyperparamètres sont un peu lâches, les données (les points) ne permettent pas de représenter correctement la fonction que l'on recherche (en rouge). Les prédictions seront en moyenne hors-contexte, par contre elles se ressemblent toutes (les différentes courbes bleues passent globalement par les mêmes points). On parle d'**underfit** (sous-évaluation) dans ce cas.\n",
    "\n",
    "\n",
    "```{image} fig/bias_variance_trade_off_overfit.png \n",
    ":alt: exemple de large variance\n",
    ":align: center\n",
    "```\n",
    "\n",
    "Les hyperparamètres sont trop précis, les exemples (les points) sont correctement acquis par le mécanisme d'apprentissage, mais les prédictions sont floues (les différentes courbes bleues sont très fluctuantes, et s'étalent sur de larges régions). On parle d'**overfit** dans ce cas."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

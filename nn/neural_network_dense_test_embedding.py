#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 28 19:24:01 2024

@author: fraschelle
"""

import numpy as np
import neural_network_dense as nn
from neural_network import NeuralNetwork
import neural_network_from_scratch as nns

seed = None
rng = np.random.default_rng(seed)
batch_size = 16
shape_input = 8
shape_hidden_layer = 3
shape_output = 8

def generate_data(batch_size: int=batch_size):
    """AND function for two boolean values."""
    x = np.zeros((batch_size, 8), dtype=int)
    pos = rng.integers(0, 7, batch_size).reshape((batch_size, 1))
    pos = np.hstack([np.arange(batch_size).reshape((batch_size, 1)), pos])
    for posx, posy in pos:
        x[(posx, posy)] = 1
    return x.astype(int), x.astype(int)

connection_1 = nn.Connection(shape_input=shape_input,
                             shape_output=shape_hidden_layer,
                             hashid='layer_in',
                             bias=False)
activation_1 = nn.Sigmoid(hashid='sigmoid_in')
connection_2 = nn.Connection(shape_input=shape_hidden_layer,
                             shape_output=shape_output,
                             hashid='layer_out',
                             bias=False)
activation_2 = nn.Sigmoid(hashid="sigmoid_out")
loss_function = nn.SquaredErrorOnIdentity()
optimizer = nn.FlatAlpha()

connection_1.children = [activation_1]
activation_1.parents = [connection_1]
activation_1.children = [connection_2]
connection_2.parents = [activation_1]
connection_2.children = [activation_2]
activation_2.parents = [connection_2]
neural_network = NeuralNetwork(input_node=connection_1,
                               output_node=activation_2,
                               loss_function=loss_function,
                               optimizer=optimizer)

neural_network.explore()

X, y_true = generate_data(100)
y_pred = neural_network(X)
neural_network.train(X, y_true)

for epoch in range(5500):
    neural_network.train(*generate_data(100))

X, y_true = generate_data(100)
y_pred = neural_network(X)
y_pred = np.round(y_pred).astype(int)
print(np.sum(y_pred == y_true))

connection_1.children = [activation_1]
activation_1.parents = [connection_1]
embedding_network = NeuralNetwork(input_node=connection_1,
                                  output_node=activation_1,
                                  loss_function=loss_function,
                                  optimizer=optimizer)

print(X, np.round(embedding_network(X)))
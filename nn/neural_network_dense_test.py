#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 28 19:24:01 2024

@author: fraschelle
"""

import numpy as np
import neural_network_dense as nn
from neural_network import NeuralNetwork
import neural_network_from_scratch as nns

seed = None
rng = np.random.default_rng(seed)
batch_size = 8
shape_input = 2
shape_hidden_layer_1 = 3
shape_hidden_layer_2 = 3
shape_output = 1

def generate_data(batch_size: int=batch_size):
    """AND function for two boolean values."""
    x = rng.choice([0, 1], (batch_size, 2))
    return x, (x[:, 0] * x[:, 1]).reshape(x.shape[0], 1)

connection_1 = nn.Connection(shape_input=shape_input,
                             shape_output=shape_hidden_layer_1,
                             hashid='layer1',
                             bias=False)
activation_1 = nn.Sigmoid(hashid='sigmoid1')
connection_2 = nn.Connection(shape_input=shape_hidden_layer_1,
                             shape_output=shape_hidden_layer_2,
                             hashid='layer2',
                             bias=False)
activation_2 = nn.Sigmoid(hashid="sigmoid2")
connection_3 = nn.Connection(shape_input=shape_hidden_layer_2,
                             shape_output=shape_output,
                             hashid='layer3',
                             bias=False)
activation_3 = nn.Sigmoid(hashid="sigmoid3")
loss_function = nn.SquaredErrorOnIdentity()
optimizer = nn.FlatAlpha()

connection_1.children = [activation_1]
activation_1.parents = [connection_1]
activation_1.children = [connection_2]
connection_2.parents = [activation_1]
connection_2.children = [activation_2]
activation_2.parents = [connection_2]
activation_2.children = [connection_3]
connection_3.parents = [activation_2]
connection_3.children = [activation_3]
activation_3.parents = [connection_3]
neural_network = NeuralNetwork(input_node=connection_1,
                               output_node=activation_3,
                               loss_function=loss_function,
                               optimizer=optimizer)

neural_network.explore()


"""Compare the values after one turn of scratch and numpy versions."""
x, y_true = ((1, 0), (0,))
weights = nns.generate_weights(shape_input,
                               shape_hidden_layer_1,
                               shape_hidden_layer_2,
                               shape_output)

connection_1.weight = np.array(weights[0])
connection_2.weight = np.array(weights[1])
connection_3.weight = np.array(weights[2])

y_pred_neural = neural_network.forward(np.array(x).reshape((1, len(x))))
neurons, gradients = nns.forward(x, weights)
y_pred_scratch = neurons[-1]

def verify(neural_numpy, scratch_tuple):
    # one needs to withdraw the mini-batch dimension, hence [0] on the neural_network side
    return np.all(np.isclose(neural_numpy[0], np.array(scratch_tuple)))
def verify_weights(neural_numpy, scratch_tuple):
    # mini-batch dimension is no more there
    return np.all(np.isclose(neural_numpy, np.array(scratch_tuple)))

# state of the network is the same at the begining
assert verify_weights(connection_1.weight, weights[0])
assert verify_weights(connection_2.weight, weights[1])
assert verify_weights(connection_3.weight, weights[2])

assert verify(y_pred_neural, y_pred_scratch)
assert verify(connection_1._input, neurons[0])
assert connection_1._error is None
assert verify(activation_1._gradient, gradients[0])
assert verify(connection_2._input, neurons[1])
assert connection_2._error is None
assert verify(activation_2._gradient, gradients[1])
assert verify(connection_3._input, neurons[2])
assert connection_3._error is None
assert verify(activation_3._gradient, gradients[2])

# loss reconnection forward -> backward
loss_scratch = nns.loss(y_true, y_pred_scratch)
loss_neural = neural_network.loss_function(y_true, y_pred_neural)
assert np.isclose(loss_scratch, loss_neural)

# backward path
errors = nns.instanciate_errors(y_true, neurons[-1], gradients[-1])
errors_scratch = nns.backward(weights, errors, gradients)
error = neural_network.loss_function.gradient(y_true, y_pred_neural)
neural_network.backward(error)
# verify the errors
assert verify(connection_1._error.T, errors_scratch[0])
assert verify(connection_2._error.T, errors_scratch[1])
assert verify(connection_3._error.T, errors_scratch[2])

# calculate the update matrices
updates_scratch = [nns.outer_product(x, e)
                   for x, e in zip(neurons[:-1], errors_scratch)]
update_1 = connection_1.update()
update_2 = connection_2.update()
update_3 = connection_3.update()
# verify the updates matrix
assert verify_weights(update_1, updates_scratch[0])
assert verify_weights(update_2, updates_scratch[1])
assert verify_weights(update_3, updates_scratch[2])

# complete train
# state of the network is the same at the begining
assert verify_weights(connection_1.weight, weights[0])
assert verify_weights(connection_2.weight, weights[1])
assert verify_weights(connection_3.weight, weights[2])
# train the network
weights_scratch, l = nns.train(x, y_true, weights)
neural_network.optimizer.alpha = 1  # error in scratch, eta is not taken into account
neural_network.train(np.array(x).reshape((1, len(x))), y_true)
# verify the weights are updated
# assert neural_network.optimizer.alpha == nns.eta
assert verify_weights(connection_1.weight, weights_scratch[0])
assert verify_weights(connection_2.weight, weights_scratch[1])
assert verify_weights(connection_3.weight, weights_scratch[2])

X, y_true = generate_data(100)
y_pred = neural_network(X)
neural_network.train(X, y_true)

for epoch in range(5500):
    neural_network.train(*generate_data(100))

X, y_true = generate_data(100)
y_pred = neural_network(X)
y_pred = np.round(y_pred).astype(int)
print(np.sum(y_pred == y_true))

def generate_data(batch_size: int=batch_size):
    """AND function for two boolean values."""
    x = rng.choice([0, 1], (batch_size, 2))
    return x, (x[:, 0] ^ x[:, 1]).reshape(x.shape[0], 1)

neural_network = NeuralNetwork(input_node=connection_1,
                               output_node=activation_3,
                               loss_function=loss_function,
                               optimizer=optimizer)

for epoch in range(8500):
    neural_network.train(*generate_data(100))

X, y_true = generate_data(100)
y_pred = neural_network(X)
y_pred = np.round(y_pred).astype(int)
print(np.sum(y_pred == y_true))
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Base classes of the neural networks.
"""

from typing import Hashable
import numpy as np

seed = None
rng = np.random.default_rng(seed)


class Node(object):
    def __init__(self,
                 parents: list=list(),
                 children: list=list(),
                 hashid: Hashable=None):
        self.parents = list(parents)
        self.children = list(children)
        self.hashid = hashid
        self.waiting = False
        return None
    def __hash__(self):
        return hash(self.hashid)
    def __repr__(self):
        return self.__class__.__name__
    def __call__(self, x):
        """Calculate the forward function."""
        raise NotImplementedError
    def forward(self, x):
        raise NotImplementedError
    def backward(self, error):
        raise NotImplementedError


class Activation(Node):
    """Non-linear fonction that records its gradient."""
    def __init__(self,
                 parents: list=list(),
                 children: list=list(),
                 hashid: Hashable=None):
        self.parents = list(parents)
        self.children = list(children)
        self.hashid = hashid
        self.waiting = False
        self._gradient = None
        return None
    def gradient(self, x):
        """Calculate the gradient of the function."""
        raise NotImplementedError
    def forward(self, x):
        self._gradient = self.gradient(x)
        return self(x)
    def backward(self, error):
        if self._gradient is None:
            raise AttributeError("first calculate the forward propagation")
        assert self._gradient.T.shape == error.shape
        new_error = self._gradient.T * error
        self._gradient = None
        return new_error


class Connection(Node):
    """A DenseLayer is mostly a matrix whose coefficients are called weights, plus an activation function. The neuron propagate to the right by activation of the matrix product. For the learning, one needs to update the weights and keep the gradient at the entry of the layer."""
    def __init__(self,
                 shape_input: int=None,
                 shape_output: int=None,
                 bias: bool=False,
                 parents: list=list(),
                 children: list=list(),
                 hashid: Hashable=None):
        self.parents = list(parents)
        self.children = list(children)
        self.hashid = hashid
        self.bias = bool(bias)
        self.shape = (int(shape_input) + int(self.bias), int(shape_output))
        # instanciate the weights
        self.weight = rng.random(self.shape)
        # in case of several inputs or outputs, waiting is True until the last
        # input or output is collected
        self.waiting = False
        # to collect forward, then backward, then update in that order
        self._input = None
        self._error = None
        return None
    def __repr__(self):
        string = self.__class__.__name__ + str(self.shape)
        string == " with bias" if self.bias else ""
        return string
    def __call__(self, x):
        """Propagate the error to the right."""
        if self.bias:
            x = np.hstack([x, np.ones((x.shape[0], 1))])
        return x @ self.weight
    def forward(self, x):
        """Propagate the neuron to the right, and store the input vector."""
        if self.bias:
            x = np.hstack([x, np.ones((x.shape[0], 1))])
        self._input = x
        return x @ self.weight
    def backward(self, error):
        """Propagate the error to the right."""
        self._error = error
        if self.bias:
            weight = self.weight[:-1, ...]
        else:
            weight = self.weight
        return weight @ error
    def calculate_update(self):
        assert self._input is not None, "must forward propagate before updating"
        assert self._error is not None, "must backward propagate before updating"
        assert self._input.shape[0] == self._error.shape[-1], "mini-batch is badly configured"
        # mini-batch average, the sum on the examples is given 
        # by the matrix product on the mini-batch dimension
        update = (self._error @ self._input).T / self._input.shape[0]
        if self.bias:
            update = np.vstack([update, np.ones((1, ) + self.shape[1:])])
        self._input = None
        self._error = None
        return update
    def update(self, new_weight):
        assert new_weight.shape == self.weight.shape
        self.weight = new_weight
        return self

class DenseLayer(Node):
    """The succession of a Connection and an Activation layer."""
    def __init__(self,
                 shape_input: int=None,
                 shape_output: int=None,
                 bias: bool=False,
                 parents: list=list(),
                 children: list=list(),
                 hashid: Hashable=None,
                 activation: Activation=Activation()):
        self.hashid = hashid
        self.connection = Connection(
            shape_input=shape_input,
            shape_output=shape_output,
            bias=bias,
            parents=parents,
            children=[activation],
            )
        self.activation = activation
        self.activation.parents = [self.connection]
        self.waiting = False
        return None
    def __repr__(self):
        return repr(self.connection) ' + ' + repr(self.activation)
    def __call__(self, x):
        return self.activation(self.connection(x))
    def forward(self, x):
        x = self.connection.forward(x)
        x = self.activation.forward(x)
        return x
    def backward(self, error):
        error = self.activation.backward(error)
        error = self.connection.backward(error)
        return error
    def calculate_update(self):
        return self.connection.calculate_update(self)
    def update(self, new_weight):
        self.connection.update(new_weight)
        return self    
    


if __name__ == '__main__':
    shape_input, shape_bias, shape_output = 12, 4, 8
    x = rng.random((shape_bias, shape_input))
    connection = Connection(shape_input=shape_input, shape_output=shape_output)
    traingen = connection.train()
    xout = next(traingen)
    xout = traingen.send(x)
    error = rng.random((shape_output, shape_bias))
    errorout = traingen.send(error)
    update = next(traingen)
    try:
        next(traingen)
    except StopIteration:
        print("ok")

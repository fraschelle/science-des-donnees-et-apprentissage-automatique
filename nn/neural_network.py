#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module contains the different orchestrating classes of the neural networks learning. 

A NeuralNetwork is a collection of Connection and Activation layers that are linked to each other in a Directed Acyclic Graph (a DAG).

The LossFunction manipulates the true and predicted values in order to correct the NeuralNetwork and learn a specific task.

An Optimizer is the object that updates the weights of the Connection layers to learn the task.
"""

from neural_network_layers import Activation, Connection


class Optimizer(object):
    """Optimizer resolve the problem of learning the neural networks down to a local minimum."""
    def update(
            self,
            neural_network):
        """Update the layers using the states of the neurons (row-like vector) and the errors (column-like vectors)."""
        raise NotImplementedError()


class LossFunction(object):
    """Calculate the loss and its derivative. Mainly the same class as an Activation, but for a double entry: y_pred and y_true rather than just x."""
    def _check(self, y_true, y_pred):
        assert y_pred.shape == y_true.shape, "y_true and y_pred must have same shape"
    def __call__(self, y_true, y_pred):
        raise NotImplementedError
    def instanciate_error(self, y_true, y_pred):
        raise NotImplementedError


class NeuralNetwork(object):
    """This is a graph representing the succession of layers and neurons, in the form of a directed acyclic graph (DAG). The entry and the exit of the graph are unique, given by the input_node and output_node variables."""
    def __init__(self,
                 input_node: Connection,
                 output_node: Activation,
                 loss_function: LossFunction,
                 optimizer: Optimizer,):
        if not isinstance(input_node, Connection):
            raise ValueError("input_node must be a Connection")
        if not isinstance(output_node, Activation):
            raise ValueError("output_node must be an Activation")
        if not isinstance(loss_function, LossFunction):
            raise ValueError("loss_function must be a LossFunction")
        if not isinstance(optimizer, Optimizer):
            raise ValueError("optimizer must be an Optimizer")
        self.input_node = input_node
        self.output_node = output_node
        self.loss_function = loss_function
        self.optimizer = optimizer
        return None
    def __call__(self, x):
        visited_nodes = set()
        queue = [(x, self.input_node)]
        while queue:
            x, current_node = queue.pop()
            # in case a node requires several visits A -> C <- B
            # C will wait untill both A and B are consumed
            if not current_node.waiting:
                visited_nodes.add(current_node)
            x = current_node(x)
            children = current_node.children
            queue += [(x, n) for n in set(children).difference(visited_nodes)]
        return x        
    def train(self, x, y_true):
        y_pred = self.forward(x)
        loss = self.loss_function(y_true, y_pred)
        error = self.loss_function.gradient(y_true, y_pred)
        self.backward(error.T)
        self.optimizer.update(self)
        return loss
    def explore(self):
        """Perform a breadth first search on the networks."""
        visited_nodes = set()
        queue = {self.input_node,}
        while queue:
            current_node = queue.pop()
            visited_nodes.add(current_node)
            children = current_node.children
            queue.update(set(children).difference(visited_nodes))
        return visited_nodes
    def forward(self, x):
        visited_nodes = set()
        queue = [(x, self.input_node)]
        while queue:
            x, current_node = queue.pop()
            if not current_node.waiting:
                visited_nodes.add(current_node)
            x = current_node.forward(x)
            children = current_node.children
            queue += [(x, n) for n in set(children).difference(visited_nodes)]
        return x
    def backward(self, error):
        visited_nodes = set()
        queue = [(error, self.output_node)]
        while queue:
            error, current_node = queue.pop()
            if not current_node.waiting:
                visited_nodes.add(current_node)
            error = current_node.backward(error)
            parents = current_node.parents
            queue += [(error, n)
                      for n in set(parents).difference(visited_nodes)]
        return error

class NeuralDAG(dict):
    """A NeuralNetwork must be a Directed Acyclic Graph (a DAG), that can represented as a dictionary. This class connects the elements of a NeuralNetwork from the dictionary of elements."""
    pass
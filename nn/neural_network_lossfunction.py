#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
LossFunction outputs the predictions of a NeuralNetwork. It also reinject the loss to the backward propagation.
"""

import numpy as np

from neural_network import LossFunction

class SquaredErrorOnIdentity(LossFunction):
    def __call__(self, y_true, y_pred):
        return np.sum((y_true - y_pred) ** 2) / 2
    def gradient(self, y_true, y_pred):
        return y_pred - y_true

class BinaryCrossEntropyOnSigmoid(LossFunction):
    def __call__(self, y_true, y_pred):
        return np.sum(y_true * np.log(y_pred) - (1-y_true) * np.log(1-y_pred))
    def gradient(self, y_true, y_pred):
        return y_pred - y_true

class CrossEntropyOnSoftmax(LossFunction):
    def __call__(self, y_true, y_pred):
        return np.sum(y_true * np.log(y_pred))
    def gradient(self, y_true, y_pred):
        return - y_true / y_pred

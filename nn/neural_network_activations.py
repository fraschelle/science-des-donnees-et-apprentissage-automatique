#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Activation function adds the non-linearity to the layers. They have no trainable parameters.
"""
import numpy as np

from neural_network_base import Activation


class Identity(Activation):
    def __call__(self, x):
        return x
    def gradient(self, x):
        return np.ones(x.shape)

class Sigmoid(Activation):
    def __call__(self, x):
        return 1 / (1 + np.exp(-x))
    def gradient(self, x):
        y = self(x)
        return y * (1 - y)

class ReLU(Activation):
    def __call__(self, x):
        return (x >= 0) * x
    def gradient(self, x):
        return (x >= 0) * 1

class Softmax(Activation):
    def __call__(self, x):
        return np.exp(-x) / np.sum(np.exp(-x), axis=-1).reshape(x.shape[0], 1)
    def gradient(self, x):
        y = self(x)
        return - y * (1 - y)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Optimizers, that calculate the different weights of the network, and their updates.
"""



from neural_network import Optimizer

class FlatLearningRate(Optimizer):
    def __init__(self, learning_rate: float=0.01):
        self.nb_calls = 0
        self.learning_rate = float(learning_rate)
        return None
    def update(self, neural_network):
        self.nb_calls += 1
        for node in neural_network.explore():
            if hasattr(node, 'update'):
                update = node.calculate_update()
                node.update(node.weight - self.learning_rate * update)
        return None
    
class Momentum(Optimizer):
    def __init__(self, learning_rate: float=0.01, momentum: float=0.05):
        self.nb_calls = 0
        self.learning_rate = float(learning_rate)
        self.momentum = float(momentum)
        if self.alpha < 0.0 or self.alpha > 1.0:
            raise ValueError("learning_rate must be float in [0.0, 1.0]")
        if self.momentum < 0.0 or self.momentum > 1.0:
            raise ValueError("momentum must be float in [0.0, 1.0]")
        return None
    def update(self, neural_network):
        self.nb_calls += 1
        for node in neural_network.explore():
            if hasattr(node, 'update'):
                update = node.calculate_update()
                node.update(node.weight - self.learning_rate * update)
        return None
    
        

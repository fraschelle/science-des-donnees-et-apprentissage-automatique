# Ressources sur les réseaux de neurones artificiels

## Cours au Collège de France

Il y a régulièrement des cours et colloque sur les réseaux de neurones artificiels depuis quelques années au [Collège de France](https://www.college-de-france.fr/fr/). L'intéret de ces cours est à la fois d'approffondir les aspects formels des disciplines, et de proposer en illustration de chaque cours (environ 1h00-30 par cours, suivant les cours) un séminaire (d'environ 1h) proposé par un chercheur de la discipliine enseignée, et venant compléter / illustrer les enjeux actuels (attention certains cours sont datés, surtout dans une discipline qui évolue aussi vite) de la thématique étudiée. La plupart des cours récents (depuis 2015) sont disponibles en vidéo de bonne qualité.

### Cours de [Science des données - Stéphane Mallat (depuis 2017)](https://www.college-de-france.fr/fr/chaire/stephane-mallat-sciences-des-donnees-chaire-statutaire)

Le cours en général est des plus intéressant, de niveau d'entrée master en mathématique appliquée. En particulier pour les réseaux de neurones artificiels : 

 - [L'apprentissage par réseaux de neurones profonds (2019)](https://www.college-de-france.fr/fr/agenda/cours/apprentissage-par-reseaux-de-neurones-profonds) discute les aspects historiques (le preceptron multicouche), la rétro-propagation de gradient (stochastique) et sa convergence, le théoreme de représentativité des réseaux de neurones etle problème d'optimisation associé. 
 - [Modèles multi-échelles et réseaux de neurones convolutifs (2020)](https://www.college-de-france.fr/fr/agenda/cours/modeles-multi-echelles-et-reseaux-de-neurones-convolutifs) introduit les notion de représentation de données parcimonieuses et de groupes de symmétrie, ainsi que les principes de représentation temps-fréquence et par ondelette, avec des applications pour l'image et le son. De nombreux résultats de cette année sont directement issus des recherches de Stéphane Mallat.


### Chaire annuelle d'[Informatique et sciences numériques](https://www.college-de-france.fr/fr/chaire-annuelle/chaire-annuelle-informatique-et-sciences-numeriques)

 - [L'apprentissage profond : une révolution en intelligence artificielle - Yann LeCun (2015)](https://www.college-de-france.fr/fr/chaire/yann-lecun-informatique-et-sciences-numeriques-chaire-annuelle) constitue un corpus d'applications (de l'époque) des réseaux de neurones profond / convolutifs, dont Yann LeCun est un acteur de premier plan (entre autre sur les réseaux de classification d'image / les réseaux convolutifs). Attention, le cours ne discute pas les avancées des réseaux profond sur les modèles de langages entre autres ... 
 - [Apprendre les langues aux machines - Benoît Sagot (2023)](https://www.college-de-france.fr/fr/chaire/benoit-sagot-informatique-et-sciences-numeriques-chaire-annuelle/events) ... une absence (justifiée par l'instant de publication du cours de LeCun) que vient justement compléter ce nouveau cours de Benoit Sagot ! On y trouve les avancées récentes sur les modèles de langages neuronaux, ainsi qu'une introduction aux méthodes d'apprentissages automatique du langage naturel avant la révolution des réseaux neuronaux. 

### Autre ressources

 - [L'IA et ses défis (2023)](https://www.college-de-france.fr/fr/agenda/colloque/ia-et-ses-defis), uncolloque plutot grand public.
 - [Réseaux de neurones, apprentissage et physique quantique - Antoine Georges (2023)](https://www.college-de-france.fr/fr/agenda/cours/reseaux-de-neurones-apprentissage-et-physique-quantique) explore les résultats théoriques à cheval entre la science des données et la physique théorique, ainsi que les applications récentes des réseaux de neurones artificiels en physique des matériaux et en physique quantique.
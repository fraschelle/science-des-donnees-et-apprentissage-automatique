#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Utilities for the neural networks calculation
"""

import numpy as np

def add_x0(x: np.array):
    """
    Add a last column of 1 on the data, in order to fit the regression coefficients.
    
    Parameters:
        x: a 2D numpy array, with data in columns (first argument of x.shape is the number of exmaples, second argument of x.shape is the number of features)
    
    Output:
        a numpy array, of shape (x.shape[0], x.shape[1] +1)
    """
    assert x.ndim == 2, "Input array must be of dimension 2"
    z = np.full(x.shape[0], 1).reshape(x.shape[0], 1)
    z = np.concatenate([x, z], axis=1)
    return z

def one_hot_encoding(labels, dimension=10):
    # Define a one-hot variable for an all-zero vector
    # with 10 dimensions (number labels from 0 to 9).
    one_hot_labels = labels[..., None] == np.arange(dimension)[None]
    # Return one-hot encoded labels.
    return one_hot_labels.astype(int)

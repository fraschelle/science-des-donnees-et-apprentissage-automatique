#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Construct a neural network made of dense layers.

A neural network is a succession of matrices (that transform input vector successively and mix their coefficients) and activation operations (that add non-linearities to the vectors). There are three passes to teach a neural network

 - forward pass, when the data is processed along the network and when gradients and vectors (called neurons in this context) are stored
 - loss calculation, that constitutes the transition between the forward and the backward pass using a loss function that evaluates the difference between the value predicted  by the neural netword and a (supposedly) true value acquired from a learner (a Gold Standard in machine learning parlance)
 - backward pass, when the error from the prediceted and true values are propagated along the gradients of the different layers from the output to the input
 - update, or traniing or learning step, that consist in updating the coefficients (called weights in this context) of the matrices in order to minimize the loss at the next step

The prediction requires only the forward-like propagation, without the storing of the gradient steps.

Below the simplifications are: 
    - all layers are dense connection plus sigmoid activation function (this is not modifiable)
    - no bias can be added to the dense connections
    - the loss function is the sum of the squared differences between the prediction and the true values (this is not modifiable)
    - the optimization (stochastic gradient descent) has no parametric weight, mini-batch is forbidden, dropout is impossible, ... in short: only the simplest stochastic gradient descent has been implemented

Yet the neural networks present interesting properties as e.g. prediction of simple (OR, AND and NOR for instance) boolean functions, oscillations of the loss function around a minimum, plot of the error, embedding of the hidden layers ... 

"""
from typing import List, NewType, Tuple
from math import exp
from random import random, randint

Matrix = NewType("Matrix", List[List[float]])
Vector = NewType("Vector", List[float])


# =============================================================================
# Matrix calculation
# =============================================================================

def matrix_difference(A: Matrix, B: Matrix)-> Matrix:
    """Point-wise difference of the two matrices A and B."""
    assert len(A) == len(B)
    assert len(A[0]) == len(B[0])
    for mat in [A, B]:
        lenref = len(mat[0])
        for line in mat[1:]:
            assert len(line) == lenref
    return [[a - b for a, b in zip(lA, lB)] for lA, lB in zip(A, B)]

def forward_product(x: Vector, W: Matrix) -> Vector:
    """Apply the dot product between a vector x and the matrix W as x.W. This is nothing but a matrix product between a matrix and a vector applied on the left of the matrix."""
    assert len(x) == len(W), f"incompatible vector and matrix: {len(x)} != {len(W)}"
    return tuple(sum(x[i]*W[i][j] for i in range(len(x))) for j in range(len(W[0])))

def backward_product(W: Matrix, x: Vector) -> Vector:
    """Apply the dot product between a vector x and a matrix W as W.x. This is nothing but a matrix product between a matrix and a vector applied on the right of the matrix."""
    assert len(x) == len(W[0]), f"incompatible vector and matrix: {len(x)} != {len(W[0])}"
    return tuple(sum(W[i][j]*x[j] for j in range(len(x))) for i in range(len(W)))

def outer_product(x: Vector, y: Vector) -> Matrix:
    """Construct the outer product (or tensorial product) between two vectors. 
    Return a matrix of size len(x) x len(y)."""
    z = [[0]*len(y) for _ in range(len(x))]
    for i in range(len(x)):
        for j in range(len(y)):
            z[i][j] = x[i] * y[j]
    return z

# =============================================================================
# Neural layer calculation 
# =============================================================================

# Choose the caracteristics of the network: activation, gradient, loss

def activation(x: Vector) -> Vector:
    """Activation function is a sigmoid. It will be the same for all layer."""
    return tuple(1/(1+exp(-x)) for x in x)

def gradient(x: Vector) -> Vector:
    """This is a fake gradient, since it uses that x is already a sigmoid, and not the input data."""
    return tuple(x*(1-x) for x in x)

def loss(y_true: Vector, y_pred: Vector) -> Vector:
    """Sum of squared difference error at the last layer."""
    return sum(pow(yp-yt, 2) / 2 for yp, yt in zip(y_pred, y_true))

def instanciate_errors(y_true: Vector, y_pred: Vector, last_grad: Vector) -> List[Vector]:
    """Calculate the error emerging from the last layer, comming from the loss function. It starts constructing a list that will then be completed for the remaining of the network during the backward pass. All other steps are the same since they connect the different layers in the backward propagation, this one is different since it connects the forward and the backward propagations."""
    assert all([len(y_true) == len(last_grad), len(y_true) == len(y_pred)]), "all vectors must have similar lengths"
    return [tuple((yp-yt)*g for yt, yp, g in zip(y_true, y_pred, last_grad))]

# Generic manipulation of the network

def instanciate_weights(*dimensions: int) -> List[Matrix]:
    """To instanciate the weights at the begining / before training."""
    def fill_matrix(r, c):
        matrix = [[0] * c for _ in range(r)]
        for i in range(r):
            for j in range(c):
                matrix[i][j] = random()
        return matrix
    return [fill_matrix(r, c) for r, c in zip(dimensions[:-1], dimensions[1:])]

def predict(x: Vector, weights: List[Vector]) -> Vector:
    """Pass the input vector x through all the layers, without storing anything during the propagation."""
    z = x
    for W in weights:
        z = activation(forward_product(z, W))
    return z

def forward(x: Vector, weights: List[Matrix]) -> Tuple[List[Vector], List[Vector]]:
    """Forward propagation along the network. Return the list of Vector at each layer (i.e. the neurons at the output of each layer) and the list of gradients of these vectors (somehow, see gradient function)."""
    neurons, gradients = [x], []
    for W in weights:
        neurons.append(activation(forward_product(neurons[-1], W)))
        gradients.append(gradient(neurons[-1]))
    return neurons, gradients

def backward(weights: List[Matrix],
             errors: List[Vector],
             gradients: List[Vector]) -> List[Vector]:
    """Backward propagation along the network. Return a list of Vector, that are the errors at the layer."""
    assert len(errors) == 1, "error must be feshly instanciated"
    assert len(weights) == len(gradients), "one gradient per weight matrix is required"
    for W, gradient in zip(weights[::-1][:-1], gradients[::-1][1:]):
        errors.append(
            tuple(e*g for e, g in zip(
                backward_product(W, errors[-1]), gradient)))
    return errors[::-1]

def calculate_updates(neurons: List[Vector], errors: List[Vector]) -> List[Matrix]:
    """Calculate"""
    return [outer_product(x, e) for x, e in zip(neurons[:-1], errors)]

# Optimization step: here it is a toy model (no mini-batch, no dropout, ...)

def update(weights: List[Matrix], updates: List[Matrix]) -> List[Matrix]:
    """Update the different weights by the updates Matrix.ces."""
    weights = [matrix_difference(weight, update)
               for weight, update in zip(weights, updates)]
    return weights

# Complete loop

def train(x: Vector, y: Vector, weights: List[Matrix]) -> Tuple[List[Matrix], float]:
    """Train the neural network given by the list of Matrix.ces weights, and two vectors input x and true output y."""
    neurons, gradients = forward(x, weights)
    l = loss(y, neurons[-1])
    errors = instanciate_errors(y, neurons[-1], gradients[-1])
    errors = backward(weights, errors, gradients)
    updates = calculate_updates(neurons, errors)
    weights = update(weights, updates)
    return weights, l


if __name__ == '__main__':
    """Learn the AND function between two bolean values."""
    def generate_data():
        """AND function for two boolean values."""
        x1 = randint(0, 1)
        x2 = randint(0, 1)
        return (x1, x2), (x1 * x2,)
    weights = instanciate_weights(2, 3, 3, 1)
    x, y = generate_data()
    y_pred = predict(x, weights)
    # train
    for _ in range(2500):
        x, y_true = generate_data()
        weights, l = train(x, y_true, weights)
    # test
    score = 0
    for _ in range(100):
        x, y_true = generate_data()
        y_out = predict(x, weights)
        y_pred = tuple(1 if y > 0.5 else 0 for y in y_out)
        score += 1 if y_pred == y_true else 0
    print("Score for the AND function: ", score)

    """Learn the XOR function between two bolean values. The configuration (2, 3, 3, 1) predicts correctly after many epochs, while (2, 3, 1) does not work."""
    def generate_data():
        """XOR function for two boolean values."""
        x1 = randint(0, 1)
        x2 = randint(0, 1)
        return (x1, x2), (x1 ^ x2,)
    weights = instanciate_weights(2, 1)
    x, y = generate_data()
    y_pred = predict(x, weights)
    # train
    for _ in range(250):
        x, y_true = generate_data()
        weights, l = train(x, y_true, weights)
    # test
    score = 0
    for _ in range(100):
        x, y_true = generate_data()
        y_out = predict(x, weights)
        y_pred = tuple(1 if y > 0.5 else 0 for y in y_out)
        score += 1 if y_pred == y_true else 0
    print("Score for the XOR function: ", score)
    
    """Study of the embedding: what is the state after the hidden layer ?
    This example shows that the learning of a number encoded in a digit (number between 0 and 7 in a one-hot encoding) to reproduce it passing throw a layer of three neurons learns (at the level of the hidden layer) to encode this number in binary base (e.g. 3 is (011) = 4*0+2.1+1.1). The score of the task is not 100%, though. This is surprising, and leads to the embedding possibilities of the neural network.
    """
    def generate_data(size: int=8):
        """IDENTITY function for one-hot encoded digit of a given size: one 1 and size times some 0's."""
        pos = randint(0, size-1)
        x = [0]*pos + [1] + [0]*(size-pos-1) + [1]
        return tuple(x), tuple(x)
    iteration = 0
    weights = instanciate_weights(9, 3, 9)
    x, y = generate_data()
    y_pred = predict(x, weights)
    # train
    for _ in range(5500):
        x, y = generate_data()
        weights, l = train(x, y, weights)
    # test
    score = 0
    for _ in range(100):
        x, y_true = generate_data()
        y_out = predict(x, weights)
        y_pred = tuple(1 if y > 0.5 else 0 for y in y_out)
        if y_pred != y_true:
            print(y_true, y_pred)
        score += 1 if y_pred == y_true else 0
    print("Score for the embedding translation: ", score)
    
    # stop the prediction at the hidden layer
    for pos in range(0, 8):
        x = [0]*pos + [1] + [0]*(8-pos-1) + [1]
        y_hidden = predict(x, weights[:-1])
        y_hidden = tuple(1 if y > 0.5 else 0 for y in y_hidden)
        print(x, y_hidden)

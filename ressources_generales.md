# Ressources générales

## (Ré)Visions de Mathématiques

Wiki-Université propose de nombreux cours de mathématique, pour la plupart fort bien illustrés. 

 - [Réseaux de neurones artificiels](https://fr.wikiversity.org/wiki/R%C3%A9seaux_de_neurones), ne rentre malheureusement pas dans les détails mathématiques
 - [Portail de probabilité et statistique](https://fr.wikiversity.org/wiki/D%C3%A9partement:Statistique_et_probabilit%C3%A9s) 
 - [Matrices et calcul matriciel](https://fr.wikiversity.org/wiki/Matrice), pour y voir plus clair dans le formalisme de la régression linéaire
 - [Analyse numérique et calcul scientifique](https://fr.wikiversity.org/wiki/Analyse_num%C3%A9rique_et_calcul_scientifique), pour la regression et la représentation des données de façon générale, il convient d'avoir quelques notions de matrices et de leur manipulation
 - [Département d'intelligence artificielle](https://fr.wikiversity.org/wiki/D%C3%A9partement:Intelligence_artificielle), malheureusement encore assez pauvre

Quelques notions clés en mathématique de l'apprentissage automatique : 

 - Une notion centrale de l'apprentissage automatique est la notion d'[optimisation](https://fr.wikipedia.org/wiki/Optimisation_(math%C3%A9matiques)), avec entre autre:
   * L'[algorithme de descente du gradient](https://fr.wikipedia.org/wiki/Algorithme_du_gradient), permet optimiser les problèmes d'apprentissage. Avec sa version [stochastique](https://fr.wikipedia.org/wiki/Algorithme_du_gradient_stochastique) utilisée surtout pour les réseaux de neurones artificiels, il constitue la base théorique de tous les problèmes simples d'apprentissage statistique.
   * La [rétro-propagation du gradient](https://fr.wikipedia.org/wiki/R%C3%A9tropropagation_du_gradient), méthode de mise à jour des coefficients matriciels dans les réseaux de neurones artificiels.
 - Le [traitement du signal](https://fr.wikipedia.org/wiki/Traitement_du_signal) et la [cybernétique](https://fr.wikipedia.org/wiki/Cybern%C3%A9tique) constituent les bases historiques de l'analyse statistique des données.
 - La [théorie de l'information de Shanon](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_l%27information) et l'[estimation par maximum de vraissemblance de Fisher](https://fr.wikipedia.org/wiki/Maximum_de_vraisemblance) constituent les bases théoriques de l'apprentissage machine.
 - Comprendre l'[inférence bayésienne](https://fr.wikipedia.org/wiki/Inf%C3%A9rence_bay%C3%A9sienne) permet de se sentir à l'aise avec les notions d'apprentissage statistique.
 - Le [dilemne biais-variance](https://fr.wikipedia.org/wiki/Dilemme_biais-variance) sur les risques de l'_overfit_ est une notion clé de la théorie de [Vapnik-Chervonenkis](https://fr.wikipedia.org/wiki/Th%C3%A9orie_de_Vapnik-Chervonenkis) sur l'apprentissage automatique.
 - L'apprentissage par renforcement peut être vu comme une application de la [théorie des jeux](https://fr.wikipedia.org/wiki/Th%C3%A9orie_des_jeux) à un monde ouvert.
 - Les [chaînes de Markov](https://fr.wikipedia.org/wiki/Cha%C3%AEne_de_Markov), en particulier les [processus de décision markoviens](https://fr.wikipedia.org/wiki/Processus_de_d%C3%A9cision_markovien) représentent les concepts derrière la prise de décision agent-environnement en interaction.
 - Pour les plus curieux, la notion de [génération de nombres aléatoires](https://fr.wikipedia.org/wiki/G%C3%A9n%C3%A9rateur_de_nombres_al%C3%A9atoires) constitue la pierre angulaire de nombreux concepts de statistiques, et mérite un détour.

## Apprentissage machine, généralités

On pourra toujours revenir à l'[article sur l'apprentissage automatique](https://fr.wikipedia.org/wiki/Apprentissage_automatique) de Wikipédia pour avoir un aperçu de toutes les notions liées, et fouiller l'encyclopédie en ligne depuis cette page. L'article sur l'[aide à la décision](https://fr.wikipedia.org/wiki/Aide_%C3%A0_la_d%C3%A9cision) est aussi très instructif.

Pour les lecteurs de langue anglaise, on peut citer la série d'articles de Wikipédia sur : 

 - un survol de l'[intelligence artificielle](https://en.wikipedia.org/wiki/Outline_of_artificial_intelligence),
 - les [applications de l'intelligence artificielle](https://en.wikipedia.org/wiki/Applications_of_artificial_intelligence),
 - la [chronologie](https://en.wikipedia.org/wiki/Timeline_of_artificial_intelligence) ou l'[histoire détaillé](https://en.wikipedia.org/wiki/Timeline_of_artificial_intelligence) de l'intelligence artificielle,
 - la [théorie des automates](https://en.wikipedia.org/wiki/Automata_theory),
 - l'[aide à la décision](https://en.wikipedia.org/wiki/Business_intelligence) (horriblement traduit par _business intelligence_ ...),

pour avoir une idée de l'ensemble colossal des idées brassées par le domaine.

### Les librairies Python

Les librairies suivantes sont en général construites sur les librairies de manipulation matrices comme [`numpy`](https://numpy.org/), qu'il convient de savoir utiliser. Dans une moindre mesure, [`scipy`](https://scipy.org/) (en particulier pour les graphes, les matrices creuses ou le traitement du signal) et [`sympy`](https://www.sympy.org/) (pour le calcul symbolique, entre autre les dérivations et les critères de satisfaction des fonctions booléennes) peuvent être des pré-requis pour les librairies plus exploratoires en apprentissage statistique.

 - `statsmodels` : [statsmodels.org](https://www.statsmodels.org) sur les estimations statistiques.
 - `scikit-learn` (ou `sklearn`) : [scikit-learn.org](https://scikit-learn.org/). Un excellent manuel d'utilisateur, avec tous les rappels mathématiques indispensables, por une librairie centrale de la fouille de données à l'apprentissage machine par optimisation (descente de gradient).
 - `pymc` : [Python Monte Carlo](https://www.pymc.io/welcome.html). Pour construire des réseaux bayésiens.
 - `catboost` : [catBoost](https://catboost.ai/) est une librairie qui ne gère que les arbres de décisions, mais permet de faire des calculs très rapidement, et de calculer sur les GPUs comme sur les CPUs. Très utilisé par les challenges de données
 - `keras` : [keras.io](https://keras.io/). Couche bas-niveau des réseaux de neuronnes artificiels. Faisait essentiellement partie de `tensorflow` jusqu'à `keras-3.0`, puis devient une librairie permettant de manipuler soit `tensorflow`, soit`torch`.
 - `tensorflow` : [tensorflow.org](https://www.tensorflow.org/). Pour construire des réseaux de neuronnes artificiels. Développé par Google. De nombreux tutoriels.
 - `pytorch` : [pytorch.org](https://pytorch.org/). Pour construire des réseaux de neuronnes artificiels. Développé par FaceBook. De nombreux tutoriels. C'est la version Python de `torch`, une librarie d'apprentissage basée sur les réseaux de neurones artificiels.
 
### Les livres virtuels / Jupyter-Book / Livres éxécutables

Quelques livres éxécutables (le présent site en est un !), ou extraits de codes sous la forme de NoteBook, ce qui permet une exploration des algorithmes

 - [Machine learning](https://mfauvel.frama.io/machine-learning/), livre éxécutable de Mathieu Fauvel, présentant un survol rapide et bien illustré de la librairie SciKitLearn.
 - [Explaining neural networks in raw Python:
lectures in Jupiter](https://bronwojtek.github.io/neuralnets-in-raw-python/). Une introduction plaisante et bien illustrée.
 - [numpy Tutorials](https://numpy.org/numpy-tutorials/). Reconstruire les algorithmes de machine learning à l'aide de la librarie `numpy` seulement.
 - Data Science from Scratch, 2nd Edition by Joel Grus, [O'Reilly Media, Inc.](https://www.oreilly.com/library/view/data-science-from/9781492041122/). Codes disponibles sur [GitHub:joelgrus/data-science-from-scratch](https://github.com/joelgrus/data-science-from-scratch). Reconstruire les algorithmes de machine learning en Python natif, sans passer par les librairies, immensément pédagogiques ! 
 - Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow, 3nd Edition by Aurélien Géron, [O'Reilly Media, Inc.](https://www.oreilly.com/library/view/hands-on-machine-learning/9781492032632/). Codes disponibles sur [GitHub:ageron/handson-ml3](https://github.com/ageron/handson-ml3).
 
### Les livres de références

Livres de théorie mathématique de l'apprentissage machine. Dans l'ordre chronologique.

 - Machine Learning, by Tom Mitchell, [McGraw Hill (1997)](https://www.cs.cmu.edu/~tom/mlbook.html). Très progressif et complet, avec un focus sur l'apprentissage de règles de décision. Ce livre date un peu, mais reste une référence d'avant l'explosion des réseaux de neurones artificiels (qu'il détaille et discute largement quand même !).
 - Information Theory, Inference, and Learning Algorithms, by David MacKay, [Cambridge University Press (2005)](https://www.inference.org.uk/mackay/itila/book.html). Une introduction très progressive, par le père des réseaux bayésiens.
 - The Elements of Statistical Learning, Data Mining, Inference, and Prediction, Second Edition, by  Trevor Hastie, Robert Tibshirani and Jerome Friedman, [Springer (2009)](https://link.springer.com/book/10.1007/978-0-387-84858-7). Une version mathématiquement plus élaborée que la précédente.
 - An Introduction to Statistical Learning with Applications in R, by  Gareth James, Daniela Witten, Trevor Hastie and Robert Tibshirani, [Springer (2013)](https://link.springer.com/book/10.1007/978-1-4614-7138-7). Un excellent survol de toutes les notions de base.
 - Reinforcement Learning, An Introduction, 2-nd edition, by Richard S. Sutton and Andrew G. Barto, [MIT press (2018)](https://mitpress.mit.edu/9780262039246/reinforcement-learning/). La bible de l'apprentissage par renforcement.
 - Artificial Intelligence: A Modern Approach, 4th US ed., by Stuart Russell and Peter Norvig, [Pearson (2020)](http://aima.cs.berkeley.edu/). Une [version française existe](http://aima.cs.berkeley.edu/translations.html) également. La bible historique, livre très lourd, mais très complet.
 - Chaire _Science des données_ de Stéphane Mallat, depuis 2018, au [Collège de France](https://www.college-de-france.fr/site/stephane-mallat/index.htm). Les cours de référence, niveau master 2 de mathématique appliquée. Ce n'est pas à proprement parler un livre (les cours sont accompagnés des notes de cours de Jean-Éric Campagne), mais le cours est un incontournable si l'on veut rentrer au coeur des problèmes de décision, d'apprentissage machine et de statistique des données.
 
## Eploration des données

 - L'écosystème [jupyter](https://jupyter.org/) (pour Julia, Python et R, les trois langages de références pour les communautés scientifiques en général et statistiques en particulier) est l'outil par excellence de visualisation, d'exploration et de partage de résultats, de données, de tutoriels, de documentations, ... 
 - L'IDE [spyder](https://www.spyder-ide.org/) est l'IDE de référence concernant les usages scientifiques de Python. Il intègre un editeur de code, une [console IPython](https://ipython.org/), un visualiseur de variables et un visualiseur de graphique, en plus de tous les autres outils standards d'une IDE (débug, complétion de code, vérificateur de code, ...), voir le [tutoriel associé à l'exploration et la visualisation des données](https://docs.spyder-ide.org/current/workshops/scientific-computing.html) sur la documentation de `spyder`.
 - L'IDE [Rstudio](https://posit.co/download/rstudio-desktop/) présente aussi des facilités d'exploration et d'analyse de données scientifiques. Basé historiquement sur le langage [R](https://www.r-project.org/) centré sur l'exploitation statistique de données, `RStudio` a évolué pour intégrer également Python. 
 - La librarie [pandas](https://pandas.pydata.org/) est la référence pour la manipulation et la visualisation des données tabulaires. 

## Visualisation des données

 - [The Python Graph Gallery](https://www.python-graph-gallery.com/). Une présentation des différents types de graphiques et de représentation des données. Assez complet, très illustré.
 - [matplotlib](https://matplotlib.org/) est le paquet de référence pour visualiser des données en Python. Sa [documentation](https://matplotlib.org/stable/plot_types/index.html) présente comment faire des graphes simples rapidement.
 - La documentation de [plotly](https://plotly.com/python/), un paquet de visualisation pour Python, est très visuel et contient de nombreuses variétés de graphes. `plotly` est une librarie orientée vers les graphes dynamiques et interactifs, il est trés lié à [dash](https://dash.plotly.com/), développé par la même équipe/ `dash` est un outil de création de _dashboards_, qui sont des tableau de bord de visualisation des données de façon interactive.
 - [seaborn](https://seaborn.pydata.org/) est une librairie orientée vers les représentations statistiques, et sa puissance tient à la visualitaion rapide de données multi-variables. Sa documentation, contient également un grand nombre de tutoriels de création de graphes.
 - [streamlit](https://streamlit.io/) est un autre outil de création de _dashboards_, beaucoup plus simple à prendre en main que `dash`, mais évidemment moins riche. Sa [documentation](https://streamlit.io/gallery) présente quelques exemple de visualisation de données, bien qu'on ai l'impression que `streamlit` serve surtout à faire des blogs un tant soit peu dynamique.
 - Parmis l'écosystème [jupyter](https://jupyter.org/), le module [ipywidget](https://ipywidgets.readthedocs.io/en/stable/) permet de gérer des notebooks interactifs simples, tandis que [jupyter-book](https://jupyterbook.org/) est l'outil de génération et de publication de contenu plus structurés (le présent site utilise cet outil).

## Ethique des données

 - Weapons of Math Destruction, by Cathy O'Neil, [Crown Books](https://en.wikipedia.org/wiki/Weapons_of_Math_Destruction)

# Science des données, apprentissage automatique et aide à la décision

Ces notes de cours survolent les notions de science des données (_data science_), d'apprentissage automatique (_machine learning_), d'apprentissage profond (_deep learning_ et _neural network_), d'apprentissage statistique, d'aide à la décision, ... 

Les notes de cours sont consultables sur [fraschelle.frama.io/science-des-donnees-et-apprentissage-automatique](https://fraschelle.frama.io/science-des-donnees-et-apprentissage-automatique)

Le dépot Git associé à ces notes de cours est accessible via [framagit:fraschelle/science-des-donnees-et-apprentissage-automatique](https://framagit.org/fraschelle/science-des-donnees-et-apprentissage-automatique)

## Mots clés associés, _Associated Keywords_

 - science des données, _data science_
 - apprentissage machine, _machine learning_
 - aide à la décision, _business intelligence_
 - réseau de neurones artificiels et apprentissage profond, _artificial neural networks_ & _deep learning_
 - apprentissage supervisé, classification et régression _supervised learning_
 - apprentissage non-supervisé, _unsupervised learning_, _clustering_
 - apprentissage par renforcement, _reinforcement learning_

## Remarques / Suggestions

N'hésitez pas à faire vos remarques en soulevant des _issues_ sur le [dépot framagit:fraschelle/science-des-donnees-et-apprentissage-automatique](https://framagit.org/fraschelle/science-des-donnees-et-apprentissage-automatique) associé à ce [JupyterBook](https://jupyterbook.org/).

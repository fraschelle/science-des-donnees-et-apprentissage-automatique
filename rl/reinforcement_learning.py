#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Reinforcement Learning
"""

from typing import List, Tuple

import gymnasium as gym

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation
matplotlib.rc('animation', html='jshtml')

class BaseAgent():
    def __init__(self, env: gym.Env, **kwargs) -> None:
        return NotImplementedError
    def reset(self, state: gym.core.ObsType) -> None:
        """Reset agent parameters, for a new game."""
        return NotImplementedError
    def play(self, env: gym.Env, state: gym.core.ObsType) -> gym.core.ActType:
        """Choose next action from environment, and actual state."""
        return NotImplementedError
    def update(self,
               actions: List[gym.core.ActType],
               states: List[gym.core.ObsType],
               rewards: List[float],
               infos: List[dict]) -> None:
        """Update learning strategy from action onto the environment."""
        return NotImplementedError

def play_game(env: gym.Env,
              agent: BaseAgent,
              nb_step_max: int = 50000,
              ) -> Tuple[
                  List[gym.core.ActType],
                  List[gym.core.ObsType],
                  List[float],
                  List[dict],
                  List[matplotlib.figure.Figure],
                  ]:
    t = 0
    state, info = env.reset()
    actions, states, rewards, infos = [], [state], [], []
    frames = []
    while t < nb_step_max:
        action = agent.play(env, state)
        state, reward, terminated, truncated, info = env.step(action)
        actions.append(action)
        states.append(state)
        rewards.append(reward)
        infos.append(info)
        frames.append(env.render())
        if terminated or truncated:
            break
        t += 1
    return actions, states, rewards, infos, frames

def learn_game(env: gym.Env,
               agent: BaseAgent,
               nb_total_games: int = 10000,
               nb_step_max: int = 150,) -> None:
    for _ in range(nb_total_games):
        state, info = env.reset()
        agent.reset(state)
        actions, states, rewards, infos, _ = play_game(env, agent, nb_step_max)
        agent.update(actions, states, rewards, infos)
    env.close()
    return None

def display_env(env):
    img = env.render()
    plt.figure(figsize=(2,2))
    plt.imshow(img)
    plt.axis("off")
    plt.show()
    return img

def plot_animation(frames: List[matplotlib.figure.Figure], 
                   repeat: bool = False, 
                   interval: int = 40) -> matplotlib.animation.FuncAnimation:
    def update_scene(num, frames, patch):
        patch.set_data(frames[num])
        return patch,
    fig = plt.figure(figsize=(2,2))
    patch = plt.imshow(frames[0])
    plt.axis('off')
    anim = animation.FuncAnimation(
        fig, update_scene, fargs=(frames, patch),
        frames=len(frames), repeat=repeat, interval=interval)
    plt.show()
    return anim

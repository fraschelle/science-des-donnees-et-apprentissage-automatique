#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 07:21:22 2024

@author: fraschelle
"""

import matplotlib as mpl
import matplotlib.pyplot as plt

nb_points = 250

def linspace(xmin=-1, xmax=1, nb_points=nb_points):
    """Construct a list of float from xmin to xmax with nb_points+1 points."""
    return [xmin + t * (xmax - xmin) / nb_points for t in range(nb_points+1)]


# see https://matplotlib.org/stable/gallery/color/named_colors.html
colors = list(mpl.colors.TABLEAU_COLORS.values())
# see https://matplotlib.org/stable/api/markers_api.html#module-matplotlib.markers
markers = ['o', 's', 'X', 'D', 'P']

def classification(features, targets):
    """Plot the 2D classification of features. Each feature is assigned to the corresponding target in the targets list."""
    assert len(features) == len(targets)
    for i, x in enumerate(set(targets)):
        mask = [pos for pos, target in enumerate(targets) if target==x]
        plt.scatter([features[i][0] for i in mask],
                    [features[i][1] for i in mask],
                    color=colors[i%len(colors)],
                    marker=markers[i%len(markers)])
    plt.show()
    return None

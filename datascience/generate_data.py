#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 07:21:22 2024

@author: fraschelle
"""

import math
import random

nb_points = 250
# which sector correspond to which class ?
# from -pi to pi, there will be len(classification_sectors) sectors
classification_sectors = [1, 1, 2, 1, 0, 1, 0, 1, 3, 4]

def radial_sectors(
    nb_points=nb_points,
    classification_sectors=classification_sectors):
    """Construct nb_points of features and targets according to the classification_sectors mapping.
    
    The classification_sectors is a list of integers. Its length represents the number of sectors. The integers represent the class of the sector.
    
    Returns
    -------
    features: List[Tuple(x1, x2)] where the floats x1 and x2 are choosen randomly in the range [-1, 1]. Depending on their angle, they will be attributed to a specific class.
    targets: List[int] represents the class (given by the integers in classification_sectors list).
    
    Example
    -------
    features, targets = radial_sectors(nb_points=25, classification_sectors=[0,1])
    # represents a binary classification with two features (x1, x2) where 0 class corresponds to x2 < 0 and class 1 corresponds to x2 > 0.
    """
    features, targets = [], []
    for _ in range(nb_points):
        x1, x2 = 2*random.random()-1, 2*random.random()-1
        features.append((x1, x2))
        # angles from the x axis
        angle = (math.atan2(x2, x1)/math.pi + 1)/2
        targets.append(classification_sectors[int(angle * len(classification_sectors))])
    return features, targets

def add_noise(targets, percent: int=10):
    "Shuffle a percent of the targets."
    for _ in range(int(len(targets)*percent/100)):
        x1, x2 = random.randint(0, len(targets)-1), random.randint(0, len(targets)-1)
        targets[x1] = targets[x2]
    return targets
